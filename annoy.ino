int piezoPin = 8;
long tone_choosen;
long time_choosen;

void setup()
{
  tone(piezoPin, 1000, 80);
  randomSeed(analogRead(5));
}

void loop()
{
  
  tone_choosen = random(3);
  time_choosen = random(60000);
  switch (tone_choosen)
  {
  case 0:
    tone_1();
    break;
  case 1:
    tone_2();
    break;
  case 2:
    tone_3();
    break;
  default:
    break;
  }
  delay(time_choosen);
}

void tone_1()
{
  tone(piezoPin, 1000, 3000);
}

void tone_2()
{
  for (int i = 0; i < 5; i++)
  {
    tone(piezoPin, 1000, 150);
    delay(300);
  }
}

void tone_3()
{
  for (int i = 0; i < 15; i++)
  {
    tone(piezoPin, 1000, 100);
    delay(100);
    tone(piezoPin, 400, 100);
    delay(100);
  }
}
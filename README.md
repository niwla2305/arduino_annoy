# arduino_annoy
A little Arduino Sketch to annoy people using a speaker connected to any Arduino compatible board.

## Setup
 * Clone this repo
 * Open it in Arduino IDE
 * Change piezoPin at top of the sketch to your speaker pin.
 * Compile and Flash!
 * Annoy people!